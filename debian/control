Source: blas
Priority: optional
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Sébastien Villemot <sebastien@debian.org>
Standards-Version: 3.9.6
Build-Depends: gfortran, debhelper (>= 9), dpkg-dev (>= 1.16.1~)
Build-Depends-Indep: texlive-latex-recommended
Section: libs
Homepage: http://www.netlib.org/blas/
Vcs-Git: git://anonscm.debian.org/debian-science/packages/blas.git
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=debian-science/packages/blas.git

Package: libblas3
Architecture: any
Provides: libblas.so.3
Depends: ${shlibs:Depends}, ${misc:Depends}, libblas-common
Description: Basic Linear Algebra Reference implementations, shared library
 BLAS (Basic Linear Algebra Subroutines) is a set of efficient
 routines for most of the basic vector and matrix operations. 
 They are widely used as the basis for other high quality linear
 algebra software, for example lapack and linpack.  This 
 implementation is the Fortran 77 reference implementation found
 at netlib. 
 .
 This package contains a shared version of the library.

Package: libblas-common
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Dependency package for all BLAS implementations
 The only purpose of this empty package is to ensure multi-arch safety of
 different BLAS implementations. See #760936 for more details.

Package: libblas-dev
Architecture: any
Section: libdevel
Provides: libblas.so
Depends: libblas3 (= ${binary:Version}), ${misc:Depends}, gfortran
Description: Basic Linear Algebra Subroutines 3, static library
 This package is a binary incompatible upgrade to the blas-dev
 package. Several minor changes to the C interface have been
 incorporated. 
 .	
 BLAS (Basic Linear Algebra Subroutines) is a set of efficient
 routines for most of the basic vector and matrix operations.
 They are widely used as the basis for other high quality linear
 algebra software, for example lapack and linpack.  This 
 implementation is the Fortran 77 reference implementation found
 at netlib. 
 .
 This package contains a static version of the library.

Package: libblas-test
Architecture: any
Section: devel
Priority: extra
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Basic Linear Algebra Subroutines 3, testing programs
 BLAS (Basic Linear Algebra Subroutines) is a set of efficient
 routines for most of the basic vector and matrix operations.
 They are widely used as the basis for other high quality linear
 algebra software, for example lapack and linpack.  This 
 implementation is the Fortran 77 reference implementation found
 at netlib. 
 .
 This package contains a set of programs which test the integrity of an
 installed blas-compatible shared library. These programs may therefore be used
 to test the libraries provided by the blas package as well as those provided
 by the libatlas3-base and libopenblas-base packages. The programs are
 dynamically linked -- one can explicitly select a library to test by setting
 the libblas.so.3 alternative, or by using the LD_LIBRARY_PATH or LD_PRELOAD
 environment variables. Likewise, one can display the library selected using
 the ldd program in an identical environment.

Package: libblas-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Description: Basic Linear Algebra Subroutines 3, documentation
 BLAS (Basic Linear Algebra Subroutines) is a set of efficient
 routines for most of the basic vector and matrix operations.
 They are widely used as the basis for other high quality linear
 algebra software, for example lapack and linpack.  This 
 implementation is the Fortran 77 reference implementation found
 at netlib. 
 .
 This package contains manual pages for the routines, and other
 supporting documentation
