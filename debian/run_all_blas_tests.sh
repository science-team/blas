#!/bin/sh

# This scripts runs all BLAS tests against the BLAS implementation currently
# selected through the alternatives system
# (see "update-alternatives --display libblas.so.3").

PATH=$PATH:/usr/lib/libblas

EXDIR=/usr/share/doc/libblas-test/examples/

rm -f *.SUMM

xccblat1
xccblat2 < $EXDIR/cin2
xccblat3 < $EXDIR/cin3
xcblat1
xcblat2 < $EXDIR/cblat2d
cat CBLAT2.SUMM
xcblat3 < $EXDIR/cblat3d
cat CBLAT3.SUMM
xdcblat1
xdcblat2 < $EXDIR/din2
xdcblat3 < $EXDIR/din3
xdblat1
xdblat2 < $EXDIR/dblat2d
cat DBLAT2.SUMM
xdblat3 < $EXDIR/dblat3d
cat DBLAT3.SUMM
xscblat1
xscblat2 < $EXDIR/sin2
xscblat3 < $EXDIR/sin3
xsblat1
xsblat2 < $EXDIR/sblat2d
cat SBLAT2.SUMM
xsblat3 < $EXDIR/sblat3d
cat SBLAT3.SUMM
xzcblat1
xzcblat2 < $EXDIR/zin2
xzcblat3 < $EXDIR/zin3
xzblat1
xzblat2 < $EXDIR/zblat2d
cat ZBLAT2.SUMM
xzblat3 < $EXDIR/zblat3d
cat ZBLAT3.SUMM
